package nl.apg.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import nl.apg.demo.data.entity.VideoDescription;
import nl.apg.demo.service.VideoDescriptionService;
import nl.apg.demo.service.VideoItemService;

@Controller
public class VideoTemplateController {

	@Autowired
	private VideoItemService videoService;
	
	@Autowired
	private VideoDescriptionService detailService;
	
	@GetMapping("/template/menu")
    public String main(Model model, @RequestParam(value = "query", required=false) String query) {
        if (query != null && !query.isEmpty()) {
			model.addAttribute("videoList", videoService.searchVideo(query));
		} else {
			model.addAttribute("videoList", videoService.getFirstTenItems());
		}
        return "tileTemplate"; 
    }
	
	@GetMapping("/template/video/{id}")
	public String getOne(Model model, @PathVariable String id){
		model.addAttribute("video", detailService.getVideoDescription(id));
		return "videoTemplate";
	}
}
